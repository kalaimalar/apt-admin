import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Blocks } from '../models/blocks.model';
import { HouseType } from '../models/houseType.model';
import { BedRoomType } from '../models/bedroomtype.model';
import { Flats } from '../models/flats.model';
import { Owners } from '../models/owners.model';
import { NotificationType } from '../models/notificationType.model';
import { Gnotification } from '../models/gnotification.model';
import { Pnotification } from '../models/pnotification.model';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  blocksCol: AngularFirestoreCollection<Blocks>;
  hosueTypeCol: AngularFirestoreCollection<HouseType>;
  bedRoomTypeCol: AngularFirestoreCollection<BedRoomType>;
  flatsCol: AngularFirestoreCollection<Flats>;
  ownersCol: AngularFirestoreCollection<Owners>;
  notificationTypeCol: AngularFirestoreCollection<NotificationType>;
  gnotificationCol: AngularFirestoreCollection<Gnotification>;
  pnotificationCol: AngularFirestoreCollection<Pnotification>;


  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    public afs: AngularFirestore
  ) { }

  async login(user) {
    try {
      await this.afAuth.signInWithEmailAndPassword(user.email, user.password).then((result) => {
        if (result) {
          this.router.navigate(['dashboard']);
        }
      }).catch(e => {
        console.log(e);
      });
    } catch (e) {
      if (e) {
        console.log(e);
      }
    }
  }


  closeAdding(main, page) {
    this.router.navigate([`${main}/${page}`]);
  }

  closeAddingAbout(main, page) {
    this.router.navigate([`manage/${main}/${page}`]);
  }

  getOwnerById(id) {
    return this.afs.collection('Owners').doc(id).valueChanges();
  }

  getHouseTypeById(id) {
    return this.afs.collection('houseTypes').doc(id).valueChanges();
  }

  getBlockById(id) {
    console.log({ id });
    return this.afs.collection('blocks').doc(id).valueChanges();
  }


  getAllFlats() {
    this.flatsCol = this.afs.collection('flats', ref => {
      return ref.orderBy('createdOn', 'desc');
    });

    return this.flatsCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const flatId = a.payload.doc.id;
        const houseType = this.getHouseTypeById(data.houseType);
        const blockName = this.getBlockById(data.blockName);
        const owner = this.getOwnerById(data.ownerId);

        return { data, flatId, houseType, blockName, owner };
      });
    }));
  }

  getAllBlocks() {
    this.blocksCol = this.afs.collection('blocks', ref => {
      return ref.orderBy('createdOn', 'desc');
    });

    return this.blocksCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const blockId = a.payload.doc.id;

        return { data, blockId };
      });
    }));
  }

  getAllBedRoomTypes() {
    this.bedRoomTypeCol = this.afs.collection('bedroomTypes', ref => {
      return ref.orderBy('createdOn', 'desc');
    });

    return this.bedRoomTypeCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const bedRoomTypeId = a.payload.doc.id;

        return { data, bedRoomTypeId };
      });
    }));
  }


  getAllHouseTypes() {
    this.hosueTypeCol = this.afs.collection('houseTypes', ref => {
      return ref.orderBy('createdOn', 'desc');
    });

    return this.hosueTypeCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const houseTypeId = a.payload.doc.id;

        return { data, houseTypeId };
      });
    }));
  }

  getAllNotificationTypes() {
    this.notificationTypeCol = this.afs.collection('notificationTypes', ref => {
      return ref.orderBy('createdOn', 'desc');
    });

    return this.notificationTypeCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const notificationTypeId = a.payload.doc.id;

        return { data, notificationTypeId };
      });
    }));
  }

  getAllGeneralNotifications() {
    this.gnotificationCol = this.afs.collection('gnotification', ref => {
      return ref.orderBy('createdOn', 'desc');
    });

    return this.gnotificationCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const notifyType = this.getNotificationType(a.payload.doc.data().type);
        const gnotificationId = a.payload.doc.id;

        return { data, gnotificationId };
      });
    }));
  }

  getPersonalNotificationByFlatId(id) {
    this.pnotificationCol = this.afs.collection('pnotification', ref => {
      return ref.where('flatId', '==', id).orderBy('createdOn', 'desc');
    });

    return this.pnotificationCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const notifyType = this.getNotificationType(a.payload.doc.data().type);
        const pnotificationId = a.payload.doc.id;

        return { data, pnotificationId };
      });
    }));
  }

  getNotificationType(id) {
    return this.afs.collection('notificationTypes').doc(id).valueChanges();
  }

  getAllOwners() {
    this.ownersCol = this.afs.collection('Owners', ref => {
      return ref.orderBy('createdOn', 'desc');
    });

    return this.ownersCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const ownerId = a.payload.doc.id;

        return { data, ownerId };
      });
    }));
  }

  routeIt(data) {
    this.router.navigate([data]);
  }

  addBlock(blockData) {
    return this.afs.collection('blocks').add({
      createdOn: new Date(),
      sort: blockData.sort,
      name: blockData.name
    })
  }

  addFlat(flatData) {
    return this.afs.collection('flats').add({
      blockName: flatData.blockName,
      flatNo: flatData.flatNo,
      houseType: flatData.houseType,
      ownerId: flatData.ownerId,
      ownerStatus: flatData.ownerStatus,
      createdOn: new Date()
    })
  }

  addBedRoomType(data) {
    console.log({ data });
    return this.afs.collection('bedroomTypes').add({
      createdOn: new Date(),
      noOfRooms: data.noOfRooms,
      sort: data.sort
    })
  }

  addHouseType(houseTypeData) {
    return this.afs.collection('houseTypes').add({
      createdOn: new Date(),
      sort: houseTypeData.sort,
      type: houseTypeData.type
    })
  }

  addNotificationType(notifyTypeData) {
    return this.afs.collection('notificationTypes').add({
      type: notifyTypeData.type,
      createdOn: new Date()
    })
  }

  addGNotification(gNotifyData) {
    return this.afs.collection('gnotification').add({
      createdOn: new Date(),
      type: gNotifyData.type,
      title: gNotifyData.title,
      msgDesc: gNotifyData.msgDesc
    })
  }

  addPNotification(pnotifyData, flatId, ownerId, tenantId) {
    return this.afs.collection('pnotification').add({
      createdOn: new Date,
      flatId: flatId,
      type: pnotifyData.type,
      msgDesc: pnotifyData.msgDesc,
      title: pnotifyData.title,
      ownerId: ownerId || null,
      tenantId: tenantId || null
    })
  }

  checkResidency(id) {
    return this.afs.collection('flats').doc(id).valueChanges();
  }

  getTenantByFlatId(id) {
    return this.afs.collection('tenants', ref => {
      return ref.where('flatId', '==', id)
    }).snapshotChanges().pipe(map(actions => {
      return actions.map(e => {
        const id = e.payload.doc.id;
        return { id }
      })
    }))
  }


  getSingleGNotification(id) {
    return this.afs.collection(`gnotification`).doc(id).valueChanges();
  }
  
  getSinglePNotification(id){
    return this.afs.collection(`pnotification`).doc(id).valueChanges();

  }

  editGNotification(notifyId, data) {
    return this.afs.collection('gnotification').doc(notifyId).update({
      createdOn: new Date(),
      title: data.title,
      msgDesc: data.msgDesc,
      type: data.type
    })
  }

  editPNotification(notifyId,data){
    return this.afs.collection('pnotification').doc(notifyId).update({
      createdOn: new Date(),
      title: data.title,
      msgDesc: data.msgDesc,
      type: data.type
    })
  }

  deleteGNotification(id) {
    return this.afs.doc(`gnotification/${id}`).delete();
  }

  deletePNotification(id) {
    return this.afs.doc(`pnotification/${id}`).delete();
  }

}
