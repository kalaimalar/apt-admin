import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddhousetypeComponent } from './addhousetype.component';

describe('AddhousetypeComponent', () => {
  let component: AddhousetypeComponent;
  let fixture: ComponentFixture<AddhousetypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddhousetypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddhousetypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
