import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HouseType } from 'src/app/models/houseType.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-addhousetype',
  templateUrl: './addhousetype.component.html',
  styleUrls: ['./addhousetype.component.css']
})
export class AddhousetypeComponent implements OnInit {

  houseType = {} as HouseType;
  @ViewChild('toanimate') toAnimate: ElementRef;


  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
  }



  onCloseAdding(main, page) {
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.dataService.closeAdding(main, page);
    });
  }

  addHouseType(houseType) {
    console.log({ houseType });
    this.dataService.addHouseType(houseType)
  }
}
