import { Component, OnInit } from '@angular/core';
import { TableData } from 'src/app/md/md-table/md-table.component';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-house-type',
  templateUrl: './house-type.component.html',
  styleUrls: ['./house-type.component.css']
})
export class HouseTypeComponent implements OnInit {

  public tableData1: TableData;
  houseTypes$: Observable<any>;

  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
    this.tableData1 = {
      headerRow: [ 'Type', 'Actions'],
      dataRows: [     ]
   };

    this.houseTypes$ = this.dataService.getAllHouseTypes();

  }

  onAddHouseType() {
    this.dataService.routeIt('/master/houseType/addhousetype');
  }

  onDeleteHouseType(houseType) {

  }

}
