import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlatsComponent } from './flats/flats.component';
import { BlocksComponent } from './blocks/blocks.component';
import { HouseTypeComponent } from './house-type/house-type.component';
import { BedroomsComponent } from './bedrooms/bedrooms.component';
import { AddflatComponent } from './flats/addflat/addflat.component';
import { AddblockComponent } from './blocks/addblock/addblock.component';
import { AddhousetypeComponent } from './house-type/addhousetype/addhousetype.component';
import { AddbedroomtypeComponent } from './bedrooms/addbedroomtype/addbedroomtype.component';
import { NotificationTypesComponent } from './notification-types/notification-types.component';
import { AddnotifytypeComponent } from './notification-types/addnotifytype/addnotifytype.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', redirectTo: '/flats', pathMatch: 'full'
      },
      {
        path: 'flats',
        component: FlatsComponent,
        children: [
          {
            path: 'addflat',
            component: AddflatComponent
          }
        ]
      },
      {
        path: 'blocks',
        component: BlocksComponent,
        children: [
          {
            path: 'addblock',
            component: AddblockComponent
          }
        ]
      },
      {
        path: 'houseType',
        component: HouseTypeComponent,
        children: [
          {
            path: 'addhousetype',
            component: AddhousetypeComponent
          }
        ]
      },
      {
        path: 'bedrooms',
        component: BedroomsComponent,
        children: [
          {
            path: 'addbedroomtype',
            component: AddbedroomtypeComponent
          }
        ]
      },
      {
        path: 'notification-types',
        component: NotificationTypesComponent,
        children: [
          {
            path: 'addnotifytype',
            component: AddnotifytypeComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
