import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { MatTableDataSource } from '@angular/material/table';
import { TableData } from '../../md/md-table/md-table.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-blocks',
  templateUrl: './blocks.component.html',
  styleUrls: ['./blocks.component.css']
})
export class BlocksComponent implements OnInit {

  public tableData1: TableData;
  blocks$: Observable<any>;

  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
    this.tableData1 = {
      headerRow: [ 'Name', 'Actions'],
      dataRows: [     ]
   };

    this.blocks$ = this.dataService.getAllBlocks();

  }


  onAddBlock() {
    this.dataService.routeIt('/master/blocks/addblock');
  }

  onDeleteBlock(block) {

  }
}
