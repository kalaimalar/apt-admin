import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Blocks } from 'src/app/models/blocks.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-addblock',
  templateUrl: './addblock.component.html',
  styleUrls: ['./addblock.component.css']
})
export class AddblockComponent implements OnInit {

  block = {} as Blocks;
  @ViewChild('toanimate') toAnimate: ElementRef;


  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
  }

  onCloseAdding(main, page) {
    console.log({ main }, { page });
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.dataService.closeAdding(main, page);
    });
  }

  addBlock(block) {
    console.log({ block });
    this.dataService.addBlock(block)
  }
}
