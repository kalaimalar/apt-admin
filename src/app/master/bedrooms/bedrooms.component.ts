import { Component, OnInit } from '@angular/core';
import { TableData } from 'src/app/md/md-table/md-table.component';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-bedrooms',
  templateUrl: './bedrooms.component.html',
  styleUrls: ['./bedrooms.component.css']
})
export class BedroomsComponent implements OnInit {

  public tableData1: TableData;
  bedrooms$: Observable<any>;

  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {

    this.tableData1 = {
      headerRow: ['Name', 'Actions'],
      dataRows: []
    };

    this.bedrooms$ = this.dataService.getAllBedRoomTypes();

  }

  onAddBedRoomType() {
    this.dataService.routeIt('/master/bedrooms/addbedroomtype');
  }

  onDeleteBedRoomType(block) {

  }

}
