import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbedroomtypeComponent } from './addbedroomtype.component';

describe('AddbedroomtypeComponent', () => {
  let component: AddbedroomtypeComponent;
  let fixture: ComponentFixture<AddbedroomtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddbedroomtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbedroomtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
