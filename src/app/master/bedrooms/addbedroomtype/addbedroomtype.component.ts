import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BedRoomType } from 'src/app/models/bedroomtype.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-addbedroomtype',
  templateUrl: './addbedroomtype.component.html',
  styleUrls: ['./addbedroomtype.component.css']
})
export class AddbedroomtypeComponent implements OnInit {

  bedroomType = {} as BedRoomType;
  @ViewChild('toanimate') toAnimate: ElementRef;

  constructor(
    public dataService:DataService
  ) { }

  ngOnInit(): void {
  }

  addBedRoomType(bedroomType){
    console.log({ bedroomType });
    this.dataService.addBedRoomType(bedroomType)
  }

  onCloseAdding(main, page) {
    console.log({ main }, { page });
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.dataService.closeAdding(main, page);
    });
  }

}
