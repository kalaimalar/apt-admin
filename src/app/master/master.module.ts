import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterRoutingModule } from './master-routing.module';
import { MasterComponent } from './master.component';
import { FlatsComponent } from './flats/flats.component';
import { BlocksComponent } from './blocks/blocks.component';
import { BedroomsComponent } from './bedrooms/bedrooms.component';
import { HouseTypeComponent } from './house-type/house-type.component';
import { AddflatComponent } from './flats/addflat/addflat.component';
import { MaterialModule } from '../app.module';
import { FormsModule } from '@angular/forms';
import { AddblockComponent } from './blocks/addblock/addblock.component';
import { AddhousetypeComponent } from './house-type/addhousetype/addhousetype.component';
import { AddbedroomtypeComponent } from './bedrooms/addbedroomtype/addbedroomtype.component';
import { NotificationTypesComponent } from './notification-types/notification-types.component';
import { AddnotifytypeComponent } from './notification-types/addnotifytype/addnotifytype.component';


@NgModule({
  declarations: [MasterComponent, FlatsComponent, BlocksComponent, BedroomsComponent, HouseTypeComponent, AddflatComponent, AddblockComponent, AddhousetypeComponent, AddbedroomtypeComponent, NotificationTypesComponent, AddnotifytypeComponent],
  imports: [
    CommonModule,
    MasterRoutingModule,
    FormsModule,
    MaterialModule
  ]
})
export class MasterModule { }
