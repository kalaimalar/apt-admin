import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Flats } from 'src/app/models/flats.model';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-addflat',
  templateUrl: './addflat.component.html',
  styleUrls: ['./addflat.component.css']
})
export class AddflatComponent implements OnInit {

  flat = {} as Flats;
  @ViewChild('toanimate') toAnimate: ElementRef;
  blocks$: Observable<any>;
  houseTypes$: Observable<any>;
  owners$: Observable<any>;
  ownerData: any[] = [];


  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
    console.log('object');
    this.blocks$ = this.dataService.getAllBlocks();
    this.houseTypes$ = this.dataService.getAllHouseTypes();
    this.owners$ = this.dataService.getAllOwners();
  }

  onCloseAdding(main, page) {
    console.log({ main }, { page });
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.dataService.closeAdding(main, page);
    });
  }


  addFlat(flat) {
    console.log({ flat });
    this.dataService.addFlat(flat).then((res) => {
      if (res) {
        this.onCloseAdding('master', 'flats');
      }
    })
  }

}
