import { Component, OnInit } from '@angular/core';
import { TableData } from './../../md/md-table/md-table.component';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-flats',
  templateUrl: './flats.component.html',
  styleUrls: ['./flats.component.css']
})
export class FlatsComponent implements OnInit {

  public tableData1: TableData;
  flats$: Observable<any>;


  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
    this.tableData1 = {
      headerRow: ['Flat No', 'Block','Flat Type','Owner', 'Action'],
      dataRows: []
    };

    this.flats$ = this.dataService.getAllFlats();
    this.flats$.subscribe((data) => {
      console.log({ data });
    })

  }

  onAddFlat() {
    this.dataService.routeIt('/master/flats/addflat');
  }

  onDeleteFlat(item) {

  }

}
