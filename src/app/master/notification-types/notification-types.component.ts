import { Component, OnInit } from '@angular/core';
import { TableData } from 'src/app/md/md-table/md-table.component';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-notification-types',
  templateUrl: './notification-types.component.html',
  styleUrls: ['./notification-types.component.css']
})
export class NotificationTypesComponent implements OnInit {

  
  public tableData1: TableData;
  notificationTypes$: Observable<any>;
  
  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
    this.tableData1 = {
      headerRow: [ 'Type', 'Actions'],
      dataRows: [     ]
   };

    this.notificationTypes$ = this.dataService.getAllNotificationTypes();
  }


  onAddNotificationType() {
    this.dataService.routeIt('/master/notification-types/addnotifytype');
  }

  onDeleteNotificationType(notificationType) {

  }

}
