import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnotifytypeComponent } from './addnotifytype.component';

describe('AddnotifytypeComponent', () => {
  let component: AddnotifytypeComponent;
  let fixture: ComponentFixture<AddnotifytypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddnotifytypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnotifytypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
