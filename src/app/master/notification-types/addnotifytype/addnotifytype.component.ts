import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NotificationType } from 'src/app/models/notificationType.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-addnotifytype',
  templateUrl: './addnotifytype.component.html',
  styleUrls: ['./addnotifytype.component.css']
})
export class AddnotifytypeComponent implements OnInit {

  notificationType = {} as NotificationType;
  @ViewChild('toanimate') toAnimate: ElementRef;


  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
  }

  onCloseAdding(main, page) {
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.dataService.closeAdding(main, page);
    });
  }

  addNotificationType(notificationType) {
    console.log({ notificationType });
    this.dataService.addNotificationType(notificationType).then((res) => {
      console.log({ res });
      if(res){
        this.onCloseAdding('master', 'notification-types')
      }
    })
  }

}
