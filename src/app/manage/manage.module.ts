import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageRoutingModule } from './manage-routing.module';
import { ManageComponent } from './manage.component';
import { NotificationComponent } from './notification/notification.component';
import { PnotificationComponent } from './pnotification/pnotification.component';
import { GnotificationComponent } from './gnotification/gnotification.component';
import { AddgnotifyComponent } from './gnotification/addgnotify/addgnotify.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { PnotificationlistComponent } from './pnotification/pnotificationlist/pnotificationlist.component';
import { AddpnotificationComponent } from './pnotification/addpnotification/addpnotification.component';
import { ViewgnotifyComponent } from './gnotification/viewgnotify/viewgnotify.component';
import { EditgnotifyComponent } from './gnotification/editgnotify/editgnotify.component';
import { EditpnotifyComponent } from './pnotification/editpnotify/editpnotify.component';
import { ViewpnotifyComponent } from './pnotification/viewpnotify/viewpnotify.component';


@NgModule({
  declarations: [ManageComponent, NotificationComponent, PnotificationComponent, GnotificationComponent, AddgnotifyComponent, PnotificationlistComponent, AddpnotificationComponent, ViewgnotifyComponent, EditgnotifyComponent, EditpnotifyComponent, ViewpnotifyComponent],
  imports: [
    CommonModule,
    ManageRoutingModule,
    FormsModule,
    MaterialModule
  ]
})
export class ManageModule { }
