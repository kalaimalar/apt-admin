import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpnotificationComponent } from './addpnotification.component';

describe('AddpnotificationComponent', () => {
  let component: AddpnotificationComponent;
  let fixture: ComponentFixture<AddpnotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddpnotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddpnotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
