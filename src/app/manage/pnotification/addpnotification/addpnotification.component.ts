import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pnotification } from 'src/app/models/pnotification.model';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-addpnotification',
  templateUrl: './addpnotification.component.html',
  styleUrls: ['./addpnotification.component.css']
})
export class AddpnotificationComponent implements OnInit {

  flatId: any;

  pnotification = {} as Pnotification;
  @ViewChild('toanimate') toAnimate: ElementRef;

  notifyType$: Observable<any>;
  ownerId: any;
  tenantId: any;

  constructor(
    public actRoute: ActivatedRoute,
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.flatId = this.actRoute.snapshot.paramMap.get('id');
    console.log('faltId', this.flatId);
    this.notifyType$ = this.dataService.getAllNotificationTypes();
    if (this.flatId) {
      this.dataService.checkResidency(this.flatId).subscribe((res: any) => {
        console.log({ res });
        if (res.ownerStatus == 'resident') {
          this.ownerId = res.ownerId;
        } else if (res.ownerStatus == 'non-resident') {
          this.dataService.getTenantByFlatId(this.flatId).subscribe((data) => {
            console.log({ data });
            this.tenantId = data[0].id;
          })
        }
      })
    }

  }


  addPNotification(pnotification) {
    console.log({ pnotification });
    this.dataService.addPNotification(pnotification, this.flatId, this.ownerId, this.tenantId).then((res) => {
      if (res) {
        this.onCloseAdding();
      }
    })
  }

  onCloseAdding() {
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.router.navigate([`/manage/notification/pnotificationlist/${this.flatId}`]);
    });
  }

}
