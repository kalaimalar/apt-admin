import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TableData } from 'src/app/md/md-table/md-table.component';
import { DataService } from 'src/app/services/data.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-pnotificationlist',
  templateUrl: './pnotificationlist.component.html',
  styleUrls: ['./pnotificationlist.component.css']
})
export class PnotificationlistComponent implements OnInit {

  flatId: any;

  public tableData1: TableData;
  personalNotification$: Observable<any>;

  constructor(
    public actRoute: ActivatedRoute,
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.flatId = this.actRoute.snapshot.paramMap.get('id');
    console.log(this.flatId);

    if (this.flatId) {

      this.tableData1 = {
        headerRow: ['Title', 'Message', 'Created On', 'Actions'],
        dataRows: []
      };

      this.personalNotification$ = this.dataService.getPersonalNotificationByFlatId(this.flatId);
    }
  }

  onAddPersonalNotification() {
    this.dataService.routeIt(`/manage/notification/pnotificationlist/${this.flatId}/addpnotification/${this.flatId}`);
  }

  onViewpNotification(item) {
    this.router.navigate([`manage/notification/pnotificationlist/${this.flatId}/viewpnotify/${item.pnotificationId}`]);

  }

  onEditpNotification(item) {
    this.router.navigate([`manage/notification/pnotificationlist/${this.flatId}/editpnotify/${item.pnotificationId}`]);

  }

  onDeletepNotification(item) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.dataService.deletePNotification(item.pnotificationId);

        swal(
          {
            title: 'Deleted!',
            text: 'Your file has been deleted.',
            type: 'success',
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          }
        );
      }
    });
  }

}
