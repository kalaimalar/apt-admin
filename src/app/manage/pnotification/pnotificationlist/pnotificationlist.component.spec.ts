import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PnotificationlistComponent } from './pnotificationlist.component';

describe('PnotificationlistComponent', () => {
  let component: PnotificationlistComponent;
  let fixture: ComponentFixture<PnotificationlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PnotificationlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PnotificationlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
