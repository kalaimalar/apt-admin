import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpnotifyComponent } from './viewpnotify.component';

describe('ViewpnotifyComponent', () => {
  let component: ViewpnotifyComponent;
  let fixture: ComponentFixture<ViewpnotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpnotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpnotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
