import { Component, OnInit } from '@angular/core';
import { TableData } from 'src/app/md/md-table/md-table.component';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { DataTableComponent } from 'src/app/tables/datatable.net/datatable.component';
import { Router } from '@angular/router';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
}

declare const $: any;

@Component({
  selector: 'app-pnotification',
  templateUrl: './pnotification.component.html',
  styleUrls: ['./pnotification.component.css']
})
export class PnotificationComponent implements OnInit {

  public tableData1: TableData;
  flats$: Observable<any>;
  public dataTable: DataTable;


  constructor(
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit(): void {

    this.dataTable = {
      headerRow: ['Sl.no', 'Flat No', 'Block', 'Flat Type', 'Owner', 'Actions'],
      footerRow: ['Sl.no', 'Flat No', 'Block', 'Flat Type', 'Owner', 'Actions']
    };
    this.flats$ = this.dataService.getAllFlats();
    this.flats$.subscribe((data) => {
      console.log({ data });
    })
  }

  sendPersonalNotification(item) {
    console.log({ item });
    console.log(item.flatId);
    this.router.navigate([`manage/notification/pnotificationlist/${item.flatId}`]);
  }
}
