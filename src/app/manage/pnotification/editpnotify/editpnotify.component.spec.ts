import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditpnotifyComponent } from './editpnotify.component';

describe('EditpnotifyComponent', () => {
  let component: EditpnotifyComponent;
  let fixture: ComponentFixture<EditpnotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditpnotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditpnotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
