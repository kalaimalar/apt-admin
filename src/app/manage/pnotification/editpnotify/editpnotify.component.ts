import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Pnotification } from 'src/app/models/pnotification.model';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-editpnotify',
  templateUrl: './editpnotify.component.html',
  styleUrls: ['./editpnotify.component.css']
})
export class EditpnotifyComponent implements OnInit {

  pnotifyId: any;
  pnotification = {} as Pnotification;
  notifyTypes$: Observable<any>;

  @ViewChild('toanimate') toAnimate: ElementRef;

  constructor(
    public actRoute: ActivatedRoute,
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.notifyTypes$ = this.dataService.getAllNotificationTypes();
    this.pnotifyId = this.actRoute.snapshot.paramMap.get('id');
    console.log(this.pnotifyId);
    if (this.pnotifyId) {
      this.dataService.getSinglePNotification(this.pnotifyId).subscribe((data: any) => {
        console.log(data);
        this.pnotification.title = data.title;
        this.pnotification.msgDesc = data.msgDesc;
        this.pnotification.type = data.type;
        this.pnotification.flatId = data.flatId;
      })
    }
  }

  onCloseAdding() {
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.router.navigate([`manage/notification/pnotificationlist/${this.pnotification.flatId}`]);
    });
  }

  EditPNotification(item) {
    console.log({ item });
    console.log(this.pnotification);
    this.dataService.editPNotification(this.pnotifyId, item).then((res) => {
      this.onCloseAdding();
    })
  }

}
