import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PnotificationComponent } from './pnotification.component';

describe('PnotificationComponent', () => {
  let component: PnotificationComponent;
  let fixture: ComponentFixture<PnotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PnotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PnotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
