import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PnotificationComponent } from './pnotification/pnotification.component';
import { GnotificationComponent } from './gnotification/gnotification.component';
import { NotificationComponent } from './notification/notification.component';
import { AddgnotifyComponent } from './gnotification/addgnotify/addgnotify.component';
import { PnotificationlistComponent } from './pnotification/pnotificationlist/pnotificationlist.component';
import { AddpnotificationComponent } from './pnotification/addpnotification/addpnotification.component';
import { ViewgnotifyComponent } from './gnotification/viewgnotify/viewgnotify.component';
import { EditgnotifyComponent } from './gnotification/editgnotify/editgnotify.component';
import { ViewpnotifyComponent } from './pnotification/viewpnotify/viewpnotify.component';
import { EditpnotifyComponent } from './pnotification/editpnotify/editpnotify.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', redirectTo: '/gnotification', pathMatch: 'full'

      },
      {
        path: 'notification',
        component: NotificationComponent,
        children: [
          {
            path: 'gnotification',
            component: GnotificationComponent,
            children: [
              {
                path: 'addgnotify',
                component: AddgnotifyComponent
              },
              {
                path: 'viewgnotify/:id',
                component: ViewgnotifyComponent
              },
              {
                path: 'editgnotify/:id',
                component: EditgnotifyComponent
              }
            ]
          },
          {
            path: 'pnotification',
            component: PnotificationComponent,
          },
          {
            path: 'pnotificationlist/:id',
            component: PnotificationlistComponent,
            children: [
              {
                path: 'addpnotification/:id',
                component: AddpnotificationComponent
              },
              {
                path: 'viewpnotify/:id',
                component: ViewpnotifyComponent
              },
              {
                path: 'editpnotify/:id',
                component: EditpnotifyComponent
              }
            ]
          }
        ]
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageRoutingModule { }
