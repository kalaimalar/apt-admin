import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditgnotifyComponent } from './editgnotify.component';

describe('EditgnotifyComponent', () => {
  let component: EditgnotifyComponent;
  let fixture: ComponentFixture<EditgnotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditgnotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditgnotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
