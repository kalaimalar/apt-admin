import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Gnotification } from 'src/app/models/gnotification.model';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-editgnotify',
  templateUrl: './editgnotify.component.html',
  styleUrls: ['./editgnotify.component.css']
})
export class EditgnotifyComponent implements OnInit {

  gnotifyId: any;
  notifyTypes$: Observable<any>;

  gnotification = {} as Gnotification;
  @ViewChild('toanimate') toAnimate: ElementRef;

  constructor(
    public actRoute: ActivatedRoute,
    public router: Router,
    public dataService: DataService
  ) { }

  ngOnInit(): void {
    this.notifyTypes$ = this.dataService.getAllNotificationTypes();
    this.gnotifyId = this.actRoute.snapshot.paramMap.get('id');
    if (this.gnotifyId) {
      this.dataService.getSingleGNotification(this.gnotifyId).subscribe((data: any) => {
        console.log(data);
        this.gnotification.title = data.title;
        this.gnotification.msgDesc = data.msgDesc;
      })
    }
  }

  onCloseAdding() {
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.router.navigate(['manage/notification/gnotification']);
    });
  }

  EditGNotification(item) {
    console.log({ item });
    console.log(this.gnotification);
    this.dataService.editGNotification(this.gnotifyId, item).then((res) => {
      this.onCloseAdding();
    })
  }

}
