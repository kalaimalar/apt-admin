import { Component, OnInit } from '@angular/core';
import { TableData } from 'src/app/md/md-table/md-table.component';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-gnotification',
  templateUrl: './gnotification.component.html',
  styleUrls: ['./gnotification.component.css']
})
export class GnotificationComponent implements OnInit {

  public tableData1: TableData;
  generalNotification$: Observable<any>;

  constructor(
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.tableData1 = {
      headerRow: ['Title', 'Message', 'Created On', 'Actions'],
      dataRows: []
    };

    this.generalNotification$ = this.dataService.getAllGeneralNotifications();

  }

  onAddGeneralNotification() {
    this.dataService.routeIt('/manage/notification/gnotification/addgnotify');
  }

  onDeletegNotification(item) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.dataService.deleteGNotification(item.gnotificationId);

        swal(
          {
            title: 'Deleted!',
            text: 'Your file has been deleted.',
            type: 'success',
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          }
        );
      }
    });
  }

  onViewgNotification(item) {
    console.log(item.gnotificationId);
    this.router.navigate([`manage/notification/gnotification/viewgnotify/${item.gnotificationId}`]);

  }

  onEditgNotification(item) {
    this.router.navigate([`manage/notification/gnotification/editgnotify/${item.gnotificationId}`]);

  }
}
