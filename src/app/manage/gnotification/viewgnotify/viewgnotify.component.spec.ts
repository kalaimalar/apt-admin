import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewgnotifyComponent } from './viewgnotify.component';

describe('ViewgnotifyComponent', () => {
  let component: ViewgnotifyComponent;
  let fixture: ComponentFixture<ViewgnotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewgnotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewgnotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
