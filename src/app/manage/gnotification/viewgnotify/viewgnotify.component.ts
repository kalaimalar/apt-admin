import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Gnotification } from 'src/app/models/gnotification.model';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-viewgnotify',
  templateUrl: './viewgnotify.component.html',
  styleUrls: ['./viewgnotify.component.css']
})
export class ViewgnotifyComponent implements OnInit {

  gnotifyId: any;
  gnotification = {} as Gnotification;
  notifyTypes$: Observable<any>;

  @ViewChild('toanimate') toAnimate: ElementRef;


  constructor(
    public actRoute: ActivatedRoute,
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.notifyTypes$ = this.dataService.getAllNotificationTypes();
    this.gnotifyId = this.actRoute.snapshot.paramMap.get('id');
    console.log(this.gnotifyId);
    if (this.gnotifyId) {
      this.dataService.getSingleGNotification(this.gnotifyId).subscribe((data: any) => {
        console.log(data);
        this.gnotification.title = data.title;
        this.gnotification.msgDesc = data.msgDesc;
        this.gnotification.type = data.type;
      })
    }
  }

  onCloseAdding() {
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.router.navigate(['manage/notification/gnotification']);
    });
  }

}
