import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddgnotifyComponent } from './addgnotify.component';

describe('AddgnotifyComponent', () => {
  let component: AddgnotifyComponent;
  let fixture: ComponentFixture<AddgnotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddgnotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddgnotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
