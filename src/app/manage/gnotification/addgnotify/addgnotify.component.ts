import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Gnotification } from 'src/app/models/gnotification.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-addgnotify',
  templateUrl: './addgnotify.component.html',
  styleUrls: ['./addgnotify.component.css']
})
export class AddgnotifyComponent implements OnInit {

  gnotification = {} as Gnotification;
  @ViewChild('toanimate') toAnimate: ElementRef;

  notifyType$: Observable<any>;

  constructor(
    public dataService: DataService
  ) { }

  ngOnInit(): void {
    this.notifyType$ = this.dataService.getAllNotificationTypes();
  }

  addGNotification(gnotification) {
    console.log({ gnotification });
    this.dataService.addGNotification(gnotification).then((res) => {
      if (res) {
        this.onCloseAdding('notification', 'gnotification');
      }
    })
  }

  onCloseAdding(main, page) {
    console.log({ main }, { page });
    this.toAnimate.nativeElement.classList.remove('fadeInRight');
    this.toAnimate.nativeElement.classList.add('fadeOutRight');
    this.toAnimate.nativeElement.addEventListener('animationend', () => {
      this.dataService.closeAddingAbout(main, page);
    });
  }

}
