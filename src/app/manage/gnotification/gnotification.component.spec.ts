import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GnotificationComponent } from './gnotification.component';

describe('GnotificationComponent', () => {
  let component: GnotificationComponent;
  let fixture: ComponentFixture<GnotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GnotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GnotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
