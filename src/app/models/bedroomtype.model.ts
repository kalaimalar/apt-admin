export interface BedRoomType {
    noOfRooms: number;
    sort: number;
    createdOn: Date;
}