export interface HouseType {
    type: string;
    sort: number;
    createdOn: Date;
}