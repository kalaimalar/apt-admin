export interface Pnotification {
    msgDesc: string;
    title: string;
    type: string;
    createdOn: Date;
    flatId: string;
    ownerId?: string;
    tenantId?: string;
}
