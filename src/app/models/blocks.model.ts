export interface Blocks {
    name: string;
    sort: number;
    createdOn: Date;
}