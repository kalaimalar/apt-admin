export interface Flats {
    blockName: string;
    createdOn: Date;
    flatNo: number;
    houseType: string;
    ownerId: string;
    ownerStatus: string;
}