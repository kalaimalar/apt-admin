export interface Gnotification {
    type: string;
    createdOn: Date;
    title: string;
    msgDesc: string;
}