export interface Owners {
    DOB: Date;
    firstName: string;
    lastName: string;
    primaryMobile: string;
    secondaryMobile: string;
    createdOn:Date;
}