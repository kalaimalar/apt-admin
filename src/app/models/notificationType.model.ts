export interface NotificationType {
    type: string;
    createdOn: Date;
}